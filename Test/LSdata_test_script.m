addpath('../')
A=LSdata.ImportFromFile('061005_Zhib6_2989K_90deg_1.ASC')
B=LSdata.ImportFromFile('test.ASC')

figure(1)
clf
figure(1)

set(gcf, 'Toolbar','none')
set(gcf, 'Position',[30 30 700 450])
set(gcf,'PaperUnits','normalized')
set(gcf, 'PaperPositionMode', 'auto');


h(1)=A.g1_plot('o')
hold all
h(2)=B.g1_plot('d')

set(gca,'LineWidth',1.5);
set(gca,'FontSize',18);
set(gca,'Units','pixels');
set(gca,'Position',[150 150 400 200]);
%set(gca,'Position',[150 150 400 400]);
set(gca,'FontName','Times');

axis([1e-8 10 -0.1 1.1])
%legend({A.label, B.label})
% set(h(:),'MarkerSize',5) 

x_label='$t$ [s]'
y_label='$f(q,t)$'
xlabel(x_label,'Fontsize',24,'interpreter','latex');
ylabel(y_label,'Fontsize',24,'interpreter','latex');
